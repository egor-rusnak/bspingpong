﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.Services
{
    public class MessageConsumerService : IDisposable
    {
        private IModel _channel;
        private IConnection _connection;
        private EventingBasicConsumer _consumer;

        public MessageConsumerService(IConnectionFactory factory, MessagingSettings settings)
        {
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            CreateExchange(settings.ExchangeName, settings.ExchangeType);
            CreateQueue(settings.QueueName, settings.RoutingKey, settings.ExchangeName);
            CreateConsumer(settings.QueueName);
        }

        private void CreateQueue(string queueName, string routingKey, string exchangeName)
        {
            _channel.QueueDeclare(queueName, false, false, false);
            _channel.QueueBind(queueName, exchangeName, routingKey);
        }

        private void CreateExchange(string exchangeName, string exchangeType)
        {
            _channel.ExchangeDeclare(exchangeName, exchangeType, true, false);
        }

        private void CreateConsumer(string queueName)
        {
            _consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(queueName, false, _consumer);

        }

        public void ListenQueue(EventHandler<BasicDeliverEventArgs> handler)
        {
            _consumer.Received += handler;
        }
        public void SetMessageState(ulong deliveryTag, bool isSuccess)
        {
            if (!isSuccess)
            {
                _channel.BasicNack(deliveryTag, false, false);
            }
            else
            {
                _channel.BasicAck(deliveryTag, false);
            }
        }

        public void Dispose()
        {
            _connection.Close();
            _channel.Close();
            _connection.Dispose();
            _channel.Dispose();
        }
    }
}

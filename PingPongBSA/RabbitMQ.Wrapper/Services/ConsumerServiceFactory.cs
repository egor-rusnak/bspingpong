﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.Services
{
    public class ConsumerServiceFactory
    {
        private IConnectionFactory _factory;
        private MessagingSettings _settings;


        public ConsumerServiceFactory(IConnectionFactory factory, MessagingSettings settings)
        {
            _factory = factory;
            _settings = settings;
        }

        public MessageConsumerService CreateMessageConsumer()
        {
            if (string.IsNullOrWhiteSpace(_settings.ExchangeName)
                || string.IsNullOrWhiteSpace(_settings.ExchangeType)
                || string.IsNullOrWhiteSpace(_settings.RoutingKey)
                || string.IsNullOrWhiteSpace(_settings.QueueName))
                throw new ArgumentException("Not enough data to create a producer");

            return new MessageConsumerService(_factory, _settings);
        }
    }
}

﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.Services
{
    public class ProducerServiceFactory
    {
        private IConnectionFactory _factory;
        private MessagingSettings _settings;


        public ProducerServiceFactory(IConnectionFactory factory, MessagingSettings settings)
        {
            _factory = factory;
            _settings = settings;
        }

        public MessageProducerService CreateMessageProducer()
        {
            if (string.IsNullOrWhiteSpace(_settings.ExchangeName) || string.IsNullOrWhiteSpace(_settings.ExchangeType) || string.IsNullOrWhiteSpace(_settings.RoutingKey))
                throw new ArgumentException("Not enough data to create a producer");
            return new MessageProducerService(_factory, _settings);
        }
    }
}

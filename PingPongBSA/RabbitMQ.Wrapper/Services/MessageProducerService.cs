﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;
using System.Text;

namespace RabbitMQ.Wrapper.Services
{
    public class MessageProducerService : IDisposable
    {
        private IModel _channel;
        private IConnection _connection;
        private string _routingKey, _exchangeName;

        public MessageProducerService(IConnectionFactory factory, MessagingSettings settings)
        {
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(settings.ExchangeName, settings.ExchangeType, true, false);
            _routingKey = settings.RoutingKey;
            _exchangeName = settings.ExchangeName;
        }

        public void SendMessageToQueue(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(_exchangeName, _routingKey, body: body);
        }
        public void Dispose()
        {
            _channel.Close();
            _connection.Close();
            _channel.Dispose();
            _connection.Dispose();
        }
    }
}

﻿namespace RabbitMQ.Wrapper.Models
{
    public class MessagingSettings
    {
        public string ExchangeName { get; set; }
        public string QueueName { get; set; }
        public string RoutingKey { get; set; }
        /// <summary>
        /// Use ExchangeType fields from RabbitMq.Client
        /// </summary>
        public string ExchangeType { get; set; }
    }
}

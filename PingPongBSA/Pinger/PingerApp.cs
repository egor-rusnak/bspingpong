﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;
using System;
using System.Text;
using System.Threading;

namespace Pinger
{
    class PingerApp : IDisposable
    {
        private readonly MessageConsumerService _consumerService;
        private readonly MessageProducerService _producerService;
        public PingerApp(IConfiguration configuration)
        {
            var connectionFactory = new ConnectionFactory()
            {
                Uri = new Uri(configuration.GetConnectionString("RabbitMq"))
            };

            _consumerService = new ConsumerServiceFactory(connectionFactory,
                new MessagingSettings
                {
                    ExchangeName = "PingPongExchange",
                    ExchangeType = ExchangeType.Direct,
                    QueueName = "ping_queue",
                    RoutingKey = "Ping"
                }).CreateMessageConsumer();

            _producerService = new ProducerServiceFactory(connectionFactory,
                new MessagingSettings
                {
                    ExchangeName = "PingPongExchange",
                    ExchangeType = ExchangeType.Direct,
                    RoutingKey = "Pong"
                }).CreateMessageProducer();


            _consumerService.ListenQueue(ProcessMessage);
        }

        private void ProcessMessage(object sender, BasicDeliverEventArgs e)
        {
            Console.WriteLine("Message received at " + DateTime.Now);

            var body = e.Body;
            string message = Encoding.UTF8.GetString(body.Span);
            Console.WriteLine("Message: " + message);

            _consumerService.SetMessageState(e.DeliveryTag, true);
            Thread.Sleep(2500);

            _producerService.SendMessageToQueue("Ping");

            Console.WriteLine("Send message back..\n==========");
        }

        public void Start()
        {
            Console.WriteLine("Sending first message of ping!");
            Console.WriteLine("To stop program, write `exit`");
            _producerService.SendMessageToQueue("Ping");
            while (Console.ReadLine() != "exit")
            {
                Console.WriteLine("To exit write `exit` word.");
            }
            this.Dispose();
        }

        public void Dispose()
        {
            _consumerService.Dispose();
            _producerService.Dispose();
        }
    }
}

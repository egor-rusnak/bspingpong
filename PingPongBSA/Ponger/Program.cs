﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {

            var configBuilder = new ConfigurationBuilder()
                        .SetBasePath(Path.Combine(AppContext.BaseDirectory))
                        .AddJsonFile("appsettings.json", optional: true);

            var configuration = configBuilder.Build();
            var app = new PongerApp(configuration);
            app.Start();

        }

    }
}
